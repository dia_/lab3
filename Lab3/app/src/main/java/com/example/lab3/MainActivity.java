package com.example.lab3;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
/**
 * @author Daria Panchenko
 */
interface Object {
    void Draw(GL10 gl10);
}

class QTextured implements Object {
    private FloatBuffer cvertexBuffer;
    private FloatBuffer tvertexBuffer;
    private ShortBuffer overtexBuffer;
    private int[] texture = new int[1];

    //создание буфера координат вершин
    private FloatBuffer createVertexCBuffer(float[] vertexCoordinates) {
        ByteBuffer vertexCBuffer = ByteBuffer.allocateDirect(Float.BYTES * vertexCoordinates.length);
        vertexCBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer vertexCFBuffer = vertexCBuffer.asFloatBuffer();
        vertexCFBuffer.put(vertexCoordinates);
        vertexCFBuffer.position(0);
        return vertexCFBuffer;
    }
    //создание буфера текстур вершин
    private FloatBuffer createVertexTBuffer() {
        ByteBuffer vertexTBuffer = ByteBuffer.allocateDirect(Float.BYTES * 8);
        vertexTBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer vertexTFBuffer = vertexTBuffer.asFloatBuffer();
        vertexTFBuffer.put(new float[] {0f, 0f, 0f, 1f, 1f, 0f, 1f, 1f});
        vertexTFBuffer.position(0);
        return vertexTFBuffer;
    }
    //порядка вершин
    private ShortBuffer createVertexOBuffer() {
        ByteBuffer vertexOBuffer = ByteBuffer.allocateDirect(Short.BYTES * 4);
        vertexOBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer vertexOSBuffer = vertexOBuffer.asShortBuffer();
        vertexOSBuffer.put(new short[] {0,1,2,3 });
        vertexOSBuffer.position(0);
        return vertexOSBuffer;
    }

    QTextured(GL10 gl10, float[] vertices, Bitmap bitmap)
    {
         cvertexBuffer = this.createVertexCBuffer(vertices);
         tvertexBuffer = this.createVertexTBuffer();
         overtexBuffer = this.createVertexOBuffer();
        //генерация имени текстуры
        gl10.glGenTextures(1, texture, 0);
        gl10.glBindTexture(GL10.GL_TEXTURE_2D, texture[0]);
        //параметры текстуры
        gl10.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D,0, bitmap,0);
    }

    /**
     * метод отрисовки объекта
     */
    @Override
    public void Draw(GL10 gl10) {
        //поддержка массивов вершин
        gl10.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //записываем размер, тип, и сам буыер с точками
        gl10.glVertexPointer(3, GL10.GL_FLOAT, GL10.GL_ZERO, this.cvertexBuffer);

        gl10.glBindTexture(GL10.GL_TEXTURE_2D, texture[0]);
        //поддержка массива координат текстуры
        gl10.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        //определяет массив координат текстуры
        gl10.glTexCoordPointer(2, GL10.GL_FLOAT, GL10.GL_ZERO, this. tvertexBuffer);
        //отрисовывает объект из буфера порядка текстур
        gl10.glDrawElements(GL10.GL_TRIANGLE_STRIP, 4, GL10.GL_UNSIGNED_SHORT, this. overtexBuffer);
    }
}

class MyRenderer implements GLSurfaceView.Renderer {
    Activity activ;
    ArrayList<Object> mObjects = new ArrayList<Object>();

    int counter = 0;

    public MyRenderer(Activity activity) {
        activ = activity;
    }
    /**
     * метод вызывается при создании поверхности
     */
    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        mObjects.addAll(Arrays.asList(
                new QTextured( gl10,
                        new float[] {
                                -1.f,-1.f,0f,
                                -1.f,1.f,0f,
                                1.f,-1.f, 0f,
                                1.f, 1.f, 0f,
                        }, //BitmapFactory позволяет создать объект Bitmap из файла.
                         BitmapFactory.decodeStream(activ.getResources().openRawResource(R.raw.sun))
                ),
                new QTextured( gl10,
                        new float[] {
                                -1.f,-1.f,0f,
                                -1.f,1.f,0f,
                                1.f,-1.f, 0f,
                                1.f, 1.f, 0f,
                        },
                        BitmapFactory.decodeStream(activ.getResources().openRawResource(R.raw.earth))
                ),
                new QTextured( gl10,
                        new float[] {
                                -1.f,-1.f,0f,
                                -1.f,1.f,0f,
                                1.f,-1.f, 0f,
                                1.f, 1.f, 0f,
                        },
                        BitmapFactory.decodeStream(activ.getResources().openRawResource(R.raw.moon))
                )
        ));
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int i, int i1) {

    }
    /**
     * метод вызывается для рисования текущего кадра
     */
    @Override
    public void onDrawFrame(GL10 gl10) {
        gl10.glClearColor(0,0,0,0);
        //очищаем буферы цвета и глубины
        gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        //сброс просмотра
        gl10.glLoadIdentity();
        //масштаб
        gl10.glScalef(0.130f, 0.130f, 0.130f);
        gl10.glEnable(GL10.GL_TEXTURE_2D);

        //возвращение элементов из указанной позиции списка
        mObjects.get(0).Draw(gl10);//sun
        mObjects.get(1).Draw(gl10);//earth
        mObjects.get(2).Draw(gl10);//moon
        counter = ++counter % 360;

        //смена координат
        float earth_x = (float) Math.cos(Math.toRadians((float) counter)) * 5;
        float earth_y = (float) Math.sin(Math.toRadians((float) counter)) * 5;

        mObjects.add(1, new QTextured( gl10,
                new float[] {
                        -1.f + earth_x,   -1.f + earth_y,  0f,
                        -1.f + earth_x,   1.f + earth_y,   0f,
                        1.f + earth_x,    -1.f + earth_y,   0f,
                        1.f + earth_x,    1.f + earth_y,   0f,
                },
                BitmapFactory.decodeStream(activ.getResources().openRawResource(R.raw.earth))
        ));

        //смена координат
        float moon_x = (float) Math.cos(Math.toRadians((float) counter * 2)) * 3 + earth_x;
        float moon_y = (float) Math.sin(Math.toRadians((float) counter * 2)) * 3 + earth_y;

        mObjects.add(2, new QTextured( gl10,
                new float[] {
                        -1.f + moon_x,   -1.f + moon_y,  0f,
                        -1.f + moon_x,   1.f + moon_y,   0f,
                        1.f + moon_x,    -1.f + moon_y,   0f,
                        1.f + moon_x,    1.f + moon_y,   0f,
                },
                 BitmapFactory.decodeStream(activ.getResources().openRawResource(R.raw.moon))
        ));
        gl10.glDisable(GL10.GL_TEXTURE_2D);
    }
}

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        GLSurfaceView g = new GLSurfaceView(this);
        g.setRenderer(new MyRenderer(this));
        g.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        setContentView(g);
    }
}